import 'package:flutter/material.dart';
import '../../product/product.dart';
class ProductItem extends StatelessWidget {
  dynamic product;
  ProductItem(this.product);
  Widget build(BuildContext context) {
    var deviceSize = MediaQuery.of(context).size;
    var width = deviceSize.width;
    var height = deviceSize.height;
    return new Container(
      // height: height * 0.5,
      // width: width * 0.4,

      padding: EdgeInsets.all(10.0),
      // backgr oundColo: Colors.white,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
        boxShadow: <BoxShadow>[
          BoxShadow(color: Color.fromRGBO(0, 0, 0, 0.15), blurRadius: 0.5)
        ],
        color: Colors.white,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        // crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            // height: width * 0.4 * 1.5,
            height: width * 0.4,
            width: width * 0.4,
            padding: EdgeInsets.only(bottom: 5.0),
            child: Image.network(
              product['featured_image'].contains('https:') ? product['featured_image'] : "https:${product['featured_image']}",
              fit: BoxFit.cover,
            ),
          ),
         InkWell(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (_context) => ProductScreen(product['handle'])));
            },
            child:  Text(
            product['title'],
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
          ),
         ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                product['price_format'],
                style: TextStyle(
                    color: Colors.amber,
                    fontWeight: FontWeight.w700,
                    fontSize: 16),
              ),
              InkWell(
                onTap: () {
                  print('tap to add');
                },
                child: Container(
                  height: 20,
                  width: 20,
                  decoration: BoxDecoration(
                    color: Colors.amber,
                    borderRadius: BorderRadius.circular(40.0),
                  ),
                  child: new Icon(
                    Icons.add,
                    color: Colors.white,
                    size: 20.0,
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
