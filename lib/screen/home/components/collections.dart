import 'package:flutter/material.dart';
import './../../product/product.dart';
import './../components/productItem.dart';
class HomeCollection extends StatelessWidget {
  List collections;
  HomeCollection(this.collections);

  @override
  Widget build(BuildContext _context) {
    double cardWidth = MediaQuery.of(_context).size.width * 0.5;
    return new GridView.count(
      padding: EdgeInsets.all(10.0),
      scrollDirection: Axis.vertical,
      crossAxisCount: 2,
      shrinkWrap: true,
      physics: ScrollPhysics(),
      crossAxisSpacing: 10.0,
      mainAxisSpacing: 10.0,
      childAspectRatio: cardWidth / (cardWidth * 1.3),
      children: collections
          .map((element) => ProductItem(element))
          .toList(),
    );
  }

 
}
