import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'components/collections.dart';
import 'dart:async';
import 'package:http/http.dart' as http;

// import '../../service/http-service.dart';
class HomeScreen extends StatefulWidget {
  HomeScreen({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List slides;
  List _collections;
  bool isLoading = true;
  final String url =
      "https://suplo-fashion.myharavan.com/?view=settings.app.json";
  final String urlCol =
      "https://suplo-fashion.myharavan.com/collections/all?view=lp-detail.json";
  

  @override
  void initState() {
    this.getData();
    this.getCollection();
    super.initState();
  }

  Future<String> getData() async {
    var responseSettings = await http.get(Uri.encodeFull(url));
    var responseCol = await http.get(Uri.encodeFull(urlCol));
    setState(() {
      var convertSettings = jsonDecode(responseSettings.body);
      var convertCollection = jsonDecode(responseCol.body);
      slides = convertSettings['home']['slider'];
      _collections = convertCollection['products'];
      isLoading = false;
    });
  }

  Future<String> getCollection() async {
    var response = await http.get(Uri.encodeFull(urlCol));
    setState(() {
      var convertData = jsonDecode(response.body);
      _collections = convertData['products'];
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    // build slider
    buildSlider() {
      return new Container(
          height: 200.0,
          child: new Swiper(
            itemBuilder: (BuildContext context, int index) {
              return new Image.network(
                this.slides[index]['src'] ?? '',
                fit: BoxFit.cover,
              );
            },
            itemHeight: 250,
            itemCount: slides.length > 0 ? slides.length : 0,
            control: new SwiperControl(),
          ));
    }

    renderHome() {
      return new ListView(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          children: <Widget>[
            if (this.slides != null) buildSlider(),
            new Container(
              margin: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
              child: Text(
                'SẢN PHẨM NỔI BÂT',
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
              ),
            ),
            if (this._collections != null)
              new Container(child: HomeCollection(this._collections)),
          ]);
    }
    
    // build home
    return this.isLoading
            ? new Center(
                child: new CircularProgressIndicator(),
              )
            : renderHome();
        
  }
}
