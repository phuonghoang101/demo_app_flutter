import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../collections/collectionDetail.dart';
class CollectionsScreen extends StatefulWidget {
  CollectionsScreen();
  @override
  _CollectionsScreenState createState() => _CollectionsScreenState();
}

class _CollectionsScreenState extends State<CollectionsScreen> {
  List collections;
  bool isLoading = true;
  String listColUrl =
      "https://suplo-fashion.myharavan.com/collections/all?view=list.dl.json";
  @override
  void initState() {
    // TODO: implement initState
    this.getListCollections();
    super.initState();
  }

  Future<String> getListCollections() async {
    var response = await http.get(Uri.encodeFull(listColUrl));

    setState(() {
      var converData = jsonDecode(response.body);
      collections = converData['list_collections'];
      isLoading = false;
    });
    print(collections);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    double cardWidth = MediaQuery.of(context).size.width * 0.5;
    return this.isLoading
        ? new Center(
            child: new CircularProgressIndicator(),
          )
        : new GridView.count(
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 15.0),
            scrollDirection: Axis.vertical,
            crossAxisCount: 2,
            shrinkWrap: true,
            physics: ScrollPhysics(),
            crossAxisSpacing: 10.0,
            mainAxisSpacing: 10.0,
            childAspectRatio: cardWidth / (cardWidth * 1.3),
            children: collections
                .map((item) => new Container(
                      padding: EdgeInsets.all(10.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5.0)),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.15),
                              blurRadius: 0.5)
                        ],
                        color: Colors.white,
                      ),
                      width: cardWidth,
                      child: CollectionItem(item),
                    ))
                .toList());
  }

  Widget CollectionItem(collection) {
    return new InkWell(
      onTap: () {
        print(collection['handle']);
        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => CollectionDetailScreen(collection['handle'], collection['id'])) );
      },
      child: new Column(
        children: <Widget>[
          Image.network(
            collection['image'],
            fit: BoxFit.cover,
          ),
          SizedBox(height: 10.0),
          Text(collection['title'])
        ],
      ),
    );
  }
}
