import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../home/components/productItem.dart';
import '../search/search.dart';

class CollectionDetailScreen extends StatefulWidget {
  String _handle;
  dynamic _id;
  CollectionDetailScreen(this._handle, this._id);
  @override
  _CollectionDetailScreenState createState() => _CollectionDetailScreenState();
}

class _CollectionDetailScreenState extends State<CollectionDetailScreen> {
  List collections;
  dynamic paginate;
  bool isLoading = true;
  String baseUrl = "https://suplo-fashion.myharavan.com";
  String sortValue = "manual";
  List sort = [
    {"label": 'Tuỳ chọn', "value": "manual"},
    {"label": 'Sản phẩm bán chạy', "value": "best-selling"},
    {"label": 'Theo bảng chữ cái A - Z', "value": "title-ascending"},
    {"label": 'Theo bảng chữ cái Z - A', "value": "title-descending"},
    {"label": 'Giá từ thấp tới cao', "value": "price-ascending"},
    {"label": 'Giá từ cao tới thấp', "value": "price-descending"},
    {"label": 'Mới nhất', "value": "created-ascending"},
    {"label": 'Cũ nhất', "value": "created-descending"}
  ];
  @override
  void initState() {
    // TODO: implement initState
    this.getCollections(widget._handle, sortValue, 1);
    super.initState();
  }

  Future<String> search(searchString) async {
    var searchUrl =
        "${baseUrl}/search?q=filter=((collectionid:product=${widget._id})&&(title:product**${searchString}))&view=app.json";
    print(searchUrl);
    var response = await http.get(Uri.encodeFull(searchUrl));

    setState(() {
      var convertSearchData = jsonDecode(response.body);
      collections = convertSearchData['products'];
      paginate = convertSearchData['paginate'];
    });
    print(collections);
  }

  Future<String> getCollections(handle, sortby, page) async {
    var url =
        "${baseUrl}/collections/${handle}?sort_by=${sortby}&page=${page}&view=lp-detail.json";
    var response = await http.get(Uri.encodeFull(url));
    setState(() {
      var convertData = jsonDecode(response.body);
      collections = convertData['products'];
      paginate = convertData['paginate'];
      isLoading = false;
    });
    // print(collections);
    // print(paginate);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    double cardWidth = MediaQuery.of(context).size.width * 0.5;

    Widget renderListProduct() {
      return new GridView.count(
          padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 15.0),
          scrollDirection: Axis.vertical,
          crossAxisCount: 2,
          shrinkWrap: true,
          physics: ScrollPhysics(),
          crossAxisSpacing: 10.0,
          mainAxisSpacing: 10.0,
          childAspectRatio: cardWidth / (cardWidth * 1.3),
          children: collections.length > 0
              ? collections
                  .map((item) => new Container(
                        // padding: EdgeInsets.all(10.0),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.15),
                                blurRadius: 0.5)
                          ],
                          color: Colors.white,
                        ),
                        width: cardWidth,
                        child: ProductItem(item),
                      ))
                  .toList()
              : <Widget>[Text('Chua co san pham nao')]);
    }

    Widget renderOptions() {
      return new Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          new Container(
            // width: cardWidth - 15,
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            child: DropdownButton(
              value: sortValue,
              hint: Text('Sap xep'),
              onChanged: (newValue) {
                setState(() {
                  sortValue = newValue;
                });
                getCollections(widget._handle, sortValue, 1);
              },
              items: sort.map<DropdownMenuItem>((value) {
                return DropdownMenuItem(
                  value: value['value'],
                  child: Text(value['label']),
                );
              }).toList(),
            ),
          ),

          //   new Container(
          //     width: cardWidth - 15,
          //     padding: EdgeInsets.symmetric(horizontal: 10.0),
          //     child: DropdownButton(
          //       value: sortValue,
          //       hint: Text('Sap xep'),
          //       onChanged: (newValue) {
          //         setState(() {
          //           sortValue = newValue;
          //         });
          //       },
          //       items: sort.map<DropdownMenuItem>((value) {
          //         return DropdownMenuItem(
          //           value: value['value'],
          //           child: Text(value['label']),
          //         );
          //       }).toList(),
          //     ),
          //   )
        ],
      );
    }

    Widget renderPaginate() {
      return new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new Container(
            width: 150.0,
            height: 30.0,
            decoration: BoxDecoration(
                color: Colors.amber,
                borderRadius: BorderRadius.circular(20.0),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.15), blurRadius: 0.5)
                ]),
            child: InkWell(
              onTap: () {
                print(paginate['current_page']);
                int current_index = int.parse("${paginate['current_page']}") ;
                print(current_index);
                getCollections(widget._handle, sortValue, current_index + 1);
              },
              child: Text(
                'Xem thêm',
                style: TextStyle(color: Colors.white, height: 1.5),
                textAlign: TextAlign.center,
              ),
            ),
          )
        ],
      );
    }

    Widget renderCollections() {
      return new ListView(
        children: <Widget>[
          renderOptions(),
          renderListProduct(),
          (paginate != null && paginate['hasNext'])
              ? renderPaginate()
              : new SizedBox(height: 0),
        ],
      );
    }

    return new Scaffold(
      appBar: new AppBar(
        title: new Container(
          margin: EdgeInsets.only(right: 30.0),
          padding: EdgeInsets.symmetric(horizontal: 15.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.elliptical(20, 20)),
            boxShadow: <BoxShadow>[
              BoxShadow(color: Color.fromRGBO(0, 0, 0, 0.15), blurRadius: 0.5)
            ],
            color: Colors.white,
          ),
          child: TextField(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => SearchScreen()));
            },
            decoration:
                InputDecoration(border: InputBorder.none, hintText: 'Tìm kiếm'),
          ),
        ),
      ),
      body: this.isLoading
          ? new Center(
              child: new CircularProgressIndicator(),
            )
          : renderCollections(),
    );
  }
}
