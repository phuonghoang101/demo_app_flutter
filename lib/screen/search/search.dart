import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../home/components/productItem.dart';

class SearchScreen extends StatefulWidget {
  // String _handle;
  // dynamic _id;
  // SearchScreen(this._handle, this._id);
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  List collections;
  dynamic paginate;
  bool isLoading = false;
  String searchString;
  String baseUrl = "https://suplo-fashion.myharavan.com";
  
  @override
  void initState() {
    super.initState();
  }

  Future<String> search(searchString, page) async {
    setState(() {
      isLoading = true;
    });
    var searchUrl =
        "${baseUrl}/search?q=filter=((collectionid:product>=0)&&(title:product**${searchString}))&page=${page}&&view=app.json";
    print(searchUrl);
    var response = await http.get(Uri.encodeFull(searchUrl));

    setState(() {
      var convertSearchData = jsonDecode(response.body);
      collections = convertSearchData['products'];
      paginate = convertSearchData['paginate'];
      isLoading = false;
    });
    print(collections);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    double cardWidth = MediaQuery.of(context).size.width * 0.5;
    Widget listProduct() {
      return new GridView.count(
          padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 15.0),
          scrollDirection: Axis.vertical,
          crossAxisCount: 2,
          shrinkWrap: true,
          physics: ScrollPhysics(),
          crossAxisSpacing: 10.0,
          mainAxisSpacing: 10.0,
          childAspectRatio: cardWidth / (cardWidth * 1.3),
          children: (collections != null && collections.length > 0)
              ? collections
                  .map((item) => new Container(
                        // padding: EdgeInsets.all(10.0),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.15),
                                blurRadius: 0.5)
                          ],
                          color: Colors.white,
                        ),
                        width: cardWidth,
                        child: ProductItem(item),
                      ))
                  .toList()
              : <Widget>[Text('Chua co san pham nao')]);
    }

    Widget renderPaginate() {
      return new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new Container(
            width: 150.0,
            height: 30.0,
            decoration: BoxDecoration(
                color: Colors.amber,
                borderRadius: BorderRadius.circular(20.0),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.15), blurRadius: 0.5)
                ]),
            child: InkWell(
              onTap: () {
                var current_index = int.parse(paginate['current_page']) ;

                print(current_index);
                search(searchString, current_index + 1);
              },
              child: Text(
                'Xem thêm',
                style: TextStyle(color: Colors.white, height: 1.5),
                textAlign: TextAlign.center,
              ),
            ),
          )
        ],
      );
    }

    Widget renderSearch() {
      
      return new ListView(
        children: <Widget>[
          SizedBox(
            height: 5.0,
          ),
          // optionSearch(),
          listProduct(),
          (paginate != null && paginate['hasNext']) ? renderPaginate() : new SizedBox(height:0),
          // (paginate != null && !paginate['hasNext']) ? renderPaginate() : new SizedBox(height:0)
        ],
      );
    }

    return new Scaffold(
      appBar: new AppBar(
        title: new Container(
          margin: EdgeInsets.only(right: 30.0),
          padding: EdgeInsets.symmetric(horizontal: 15.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.elliptical(20, 20)),
            boxShadow: <BoxShadow>[
              BoxShadow(color: Color.fromRGBO(0, 0, 0, 0.15), blurRadius: 0.5)
            ],
            color: Colors.white,
          ),
          child: TextField(
            onChanged: (value) {
              setState(() {
                searchString = value;
              });
              search(searchString, 1);
            },
            decoration: InputDecoration(
                border: InputBorder.none, hintText: 'Enter a search term'),
          ),
        ),
      ),
      body: this.isLoading
          ? new Center(
              child: new CircularProgressIndicator(),
            )
          : renderSearch(),
    );
  }
}
