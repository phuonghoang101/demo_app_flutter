import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_html_widget/flutter_html_widget.dart';
import 'package:localstorage/localstorage.dart';
import '../home/components/productItem.dart';

class ProductScreen extends StatefulWidget {
  String _handle;

  ProductScreen(this._handle) : super();
  @override
  _ProductScreenState createState() => _ProductScreenState();
}

class _ProductScreenState extends State<ProductScreen> {
  static final TextEditingController _textController = TextEditingController();
  final LocalStorage storage = new LocalStorage('demo_app');
  List productSeen = new List();
  bool duplicate = false;
  dynamic product;
  dynamic variant;
  List variants;
  bool isLoading = true;
  void initState() {
    this.getProduct(widget._handle);
    _textController.text = '1';
    super.initState();
  }

  getProduct(handle) async {
    var url =
        "https://suplo-fashion.myharavan.com/products/${handle}?view=app.json";
    var response = await http.get(Uri.encodeFull(url));
    setState(() {
      var convertDataToJson = jsonDecode(response.body);
      product = convertDataToJson['product'];
      variants = convertDataToJson['product']['variants'];
      // product.toList();
      isLoading = false;
    });
    saveProduct(this.product);
    // print(this.product);
    // await print(_collections);
  }

  saveProduct(product) {
    // print(product);
    storage.deleteItem('product_seen');
    var product_format = {
      "id": product['id'],
      "title": product['title'],
      "handle": product['handle'],
      "price_format": product['price_format'],
      "compare_at_price_format": "${product['compare_at_price_format']}",
      "featured_image": "${product['images'][0]}"
    };
    var data = storage.getItem('product_seen');
    if (data is List) {
      productSeen = data;
      print('data');
      print(data);
      productSeen.forEach((item) => {
            if (item['id'] == product_format['id'])
              {
                setState(() {
                  duplicate = true;
                })
              }
          });
      print(duplicate);

      if (!duplicate) {
        productSeen.add(product_format);
      }
    } else {
      productSeen.add(product_format);
    }

    storage.setItem('product_seen', productSeen);

    // print('productSeen');
    // print(productSeen);
  }

  @override
  Widget build(BuildContext context) {
    double widthDevice = MediaQuery.of(context).size.width;
    renderOptionVariant(variant) {
      return new Container(
          width: 100.0,
          height: 30.0,
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey[50], width: 0.5),
          ),
          child: Row(children: <Widget>[
            new Container(
              width: 30,
              height: 30,
              child: Image.network(
                variant['image'],
                fit: BoxFit.cover,
              ),
            ),
            Text(variant['option1']),
          ]));
    }

    renderModalProduct() {
      return Container(
        padding: EdgeInsets.all(10.0),
        child: Column(children: <Widget>[
          new Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                  width: 100.0,
                  height: 100.0,
                  margin: EdgeInsets.only(right: 10.0),
                  child: Image.network(variants[0]['image'], fit: BoxFit.cover),
                ),
                new Container(
                    width: widthDevice - 130,
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          product['title'],
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w600),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Text(variants[0]['title']),
                        SizedBox(
                          height: 5.0,
                        ),
                        Text(variants[0]['price_format'],
                            style: TextStyle(
                                color: Colors.amber,
                                fontSize: 18,
                                fontWeight: FontWeight.bold)),
                      ],
                    ))
              ]),
          //
          SizedBox(height: 10.0),
          new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text('Số luong'),
              new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  InkWell(
                    child: Icon(Icons.remove),
                    onTap: () => {},
                  ),
                  new Container(
                    width: 40,
                    height: 30,
                    child: TextField(
                      textAlign: TextAlign.center,
                      controller: _textController,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        enabledBorder: InputBorder.none,
                        focusedBorder: InputBorder.none,
                      ),
                    ),
                  ),
                  InkWell(
                    child: Icon(Icons.add),
                    onTap: () => {},
                  ),
                ],
              )
            ],
          ),
          SizedBox(height: 10),
          new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text('Thành tiền'),
              Text('1,200,000đ',
                  style: TextStyle(
                      color: Colors.amber,
                      fontSize: 16,
                      fontWeight: FontWeight.w600))
            ],
          ),
          SizedBox(
            height: 30.0,
          ),
          new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              new Container(
                width: (widthDevice - 30) / 2,
                height: 32,
                // padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.elliptical(20, 20)),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: Color.fromRGBO(0, 0, 0, 0.15), blurRadius: 0.5)
                    ],
                    color: Colors.white,
                  ),
                  child: InkWell(
                      onTap: () {},
                      // textColor: Colors.amber,
                      child: Text(
                        'Thêm giỏ hàng',
                        style: TextStyle(
                            color: Colors.amber, fontSize: 16, height: 1.4),
                        textAlign: TextAlign.center,
                      )),
                ),
              ),
              new Container(
                width: (widthDevice - 40) / 2,
                height: 32,
                // padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.elliptical(20, 20)),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: Color.fromRGBO(0, 0, 0, 0.15), blurRadius: 0.5)
                    ],
                    color: Colors.amber,
                  ),
                  child: InkWell(
                      onTap: () {},
                      // textColor: Colors.amber,
                      child: Text(
                        'Mua ngay',
                        style: TextStyle(
                            color: Colors.white, fontSize: 16, height: 1.4),
                        textAlign: TextAlign.center,
                      )),
                ),
              ),
            ],
          )
        ]),
      );
    }

    renderProductSeen() {
      return new Container(
        height: (widthDevice * 1.35) / 2,
        color: Colors.white,
        child: ListView(
          scrollDirection: Axis.horizontal,
          // shrinkWrap: true,
          children: productSeen
              .map(
                (item) => Container(
                    width: (widthDevice - 30) / 2,
                    height: (widthDevice - 30) * 1.3 / 2,
                    margin:
                        EdgeInsets.only(top: 10.0, bottom: 10.0, right: 10.0),
                    child: ProductItem(item)),
              )
              .toList(),
        ),
      );
    }

    renderProduct() {
      return new Stack(children: <Widget>[
        new Container(
            padding: EdgeInsets.only(top: 10, bottom: 30),
            color: Colors.blueGrey[40],
            child: new ListView(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              children: <Widget>[
                new Container(
                  // margin: EdgeInsets.only(bottom: 20.0),
                  color: Colors.white,
                  height: widthDevice,
                  child: new Swiper(
                    itemBuilder: (BuildContext contex, int index) {
                      return new Image.network(product['images'][index],
                          fit: BoxFit.cover);
                    },
                    itemCount: product['images'].length > 0
                        ? product['images'].length
                        : 0,
                    control: new SwiperControl(),
                  ),
                ),
                new Container(
                  color: Colors.white,
                  margin: EdgeInsets.only(bottom: 10),
                  padding: EdgeInsets.all(15.0),
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    //  physics: ScrollPhysics(),
                    children: <Widget>[
                      Text(
                        product['title'],
                        textAlign: TextAlign.left,
                        style: TextStyle(fontSize: 16),
                      ),
                      new Container(
                        margin: EdgeInsets.only(top: 5.0, bottom: 5.0),
                        child: new Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(right: 10.0),
                              child: Text(
                                product['price_format'],
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.w700),
                              ),
                            ),
                            Text(
                              product['compare_at_price_format'],
                              style: TextStyle(
                                  decoration: TextDecoration.lineThrough,
                                  color: Colors.grey),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                new Container(
                  color: Colors.white,
                  padding: EdgeInsets.all(15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(bottom: 10),
                        child: Text('Thông tin chi tiết',
                            style:
                                TextStyle(fontSize: 16, color: Colors.black)),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                        decoration: BoxDecoration(
                            border: new Border(
                                bottom: BorderSide(
                                    width: 0.50, color: Colors.grey[300]))),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              'Thương hiệu',
                              style: TextStyle(color: Colors.grey),
                            ),
                            Text(product['vendor'])
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                        decoration: BoxDecoration(
                            border: new Border(
                                bottom: BorderSide(
                                    width: 0.50, color: Colors.grey[300]))),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              'Loại sản phẩm',
                              style: TextStyle(color: Colors.grey),
                            ),
                            Text(product['product_type'])
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                new Container(
                  padding: EdgeInsets.all(15.0),
                  color: Colors.white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(bottom: 15),
                        child: Text('Mô tả sản phẩm',
                            style:
                                TextStyle(color: Colors.black, fontSize: 16)),
                      ),
                      new HtmlWidget(html: product['body_html'])
                    ],
                  ),
                ),
                SizedBox(
                  height: 15.0,
                ),
                new ListView(
                    shrinkWrap: true,
                    physics: ScrollPhysics(),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    children: <Widget>[
                      new Text('Sản phẩm đã xem',
                          style: TextStyle(fontSize: 16, color: Colors.black)),
                      renderProductSeen()
                    ]),
              ],
            )),
        Positioned(
          bottom: 0,
          child: new Container(
              width: widthDevice,
              color: Colors.white,
              padding: EdgeInsets.all(10.0),
              child: RaisedButton(
                onPressed: () {
                  showModalBottomSheet(
                      context: context,
                      builder: (BuildContext context) {
                        return renderModalProduct();
                      });
                },
                child:
                    Text('Thêm vào giỏ hàng', style: TextStyle(fontSize: 16)),
                color: Colors.amber,
                textColor: Colors.white,
              )),
        )
      ]);
    }

    return Scaffold(
        appBar: AppBar(title: Text('Product detail')),
        body: isLoading
            ? new Center(child: new CircularProgressIndicator())
            : renderProduct());
  }
}
